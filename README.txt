Simplenews Overview
===================

Please do not use the master branch, instead use the branch corresponding to your version of drupal.

For example:

git checkout -b 6.x-1.x